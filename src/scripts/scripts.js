'use strict';

window.addEventListener("DOMContentLoaded", () => {

  // Transition Scroll ---------------------------
  const scrollLinks = document.querySelectorAll('.js-link-scroll');

  scrollLinks.forEach(anchor => {

    anchor.addEventListener('click', function (e) {
      e.preventDefault();

      const anchorHref = anchor.getAttribute('href'),
        elementPosition = document.querySelector(anchorHref).offsetTop,
        headerHeight = document.querySelector('.header').clientHeight;

      window.scrollTo({
        top: elementPosition - headerHeight, //add your necessary value
        behavior: "smooth"  //Smooth transition to roll
      });
    });
  });

  // End Transition Scroll ---------------------------
});
